
let string1 = "zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "javascript";

//Template Literals are part of JS ES6 updates
    //``-backticks
    // ${} - placeholder
let sentence = `${string1} ${string2} ${string3} ${string4} ${string5}`;
console.log(sentence);

console.log(Math.pow(5,3));

//ES6 Updates
//EcmaScript6 or ES6 is the new version of javascript
//JavaScript is formally known as EcmaScript
//ECMA - European Computer Manufacturers Association Script
    //ECMAScript standard defines the rules, details, and guidelines that the scripting language must observe to be considered ECMAScript compliant.

//Exponent Operator (**)  -ES6 update;
let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);

//Template Literals with JS Expression
let sentence2 =`the result of the five power of 2 is ${5**2}`;
console.log(sentence2);


//Array destructuring - this will allow us to save array items in a variable;

let array  = ["Kobe", "lebron", "Shaq", "Westbrook"];

console.log(array[2]);//Shaq

// //array destructuring
let lakerPlayer1 = array[3];
let lakerPlayer2 = array[0];

const [kobe, lebron, shaq, westbrook] = array;
console.log(kobe);
console.log(lebron);
console.log(shaq);
console.log(westbrook);


// array destructuring declaring only "Westbrook" to westbrook
// const [ , , ,westbrook] = array;
// console.log(westbrook)//Westbrook

let array2 = ["Curry", "Lillard", "Paul", "Irving"];

const [pG1, pG2, pG3, pG4] = array2;

console.log(pG1)
console.log(pG2)
console.log(pG3)
console.log(pG4)

let bandMembers = ["Hayley", "Zac", "Jeremy", "Taylor"];
const [vocals,lead,,bass] = bandMembers;

console.log(vocals);
console.log(lead);
console.log(bass);

//NOTE: order matters in array destructuring
    //you can skip an item by adding another separator (,) but no variable name

//syntax: const/let [var1, var2] = array;

//Object Destructuring
    //this will allow us to destructure an object by allowing us to add athe vlaues of an object's property into respective variables.

let person = {
    name: "Jeremy Davis",
    birthday: "September 12, 1989",
    age:32
};

let sentence3 = `Hi I am ${person.name}`;
console.log(sentence3);

const {age, firstName, birthday} = person;

console.log(age);//32
console.log(firstName);
//undefined = firstName is not a property of person.object
console.log(birthday);//September 12, 1989

//NOTE: order does not matter in object destructuring

let pokemon1 = {
    name: "Charmander",
    level: 11,
    type: "Fire",
    moves: ["Ember", "Scratch", "Leer"]
};

const {name, level, type, moves} =pokemon1;

console.log(`My pokemon ${name}, it is in level ${level}. It is a ${type} type. it's moves are ${moves}`);


//Arrow Functions
    //Arrow functions are alternative way of writing functions. however, arrow functions have signifiant pros and cons agianst the use of traditional function

function displayMSG () {
    console.log("Hello World")
};

const hello = () =>{
    console.log("Hello World from an arrow Function")
}
displayMSG();
hello();

const greet = (person) =>{
    console.log(`hi ${person.name}`)
};
greet(person);

//Implicit Return
    //allows us to return a value without the use of return keyword.
    //Implicit return only works with one-liner

    const addNum = (num1, num2) =>num1 + num2;
    let sum = addNum(55,60);
    console.log(sum);

// let band = bandMembers.forEach((person)=>{statement})

//this keyword

let protagonist = {
    name: "Cloud Strife",
    occupation: "Soldier",
    //traditional method would have this keyword refer to the parent object
    greet: function () {
        console.log(this);
        console.log(`hi i am ${this.name}`)
    },
    //arrow function refer to the global window
    introduce: () => {
        console.log(this);
        console.log(`I work as ${this.occupation}`)
    }
}
protagonist.greet();
protagonist.introduce();

//Class-Base Objects Blueprints
    //in Javascript, classes ar templates of objects.
    //we can create objects out of the use of classes.
    //Before the introduction of Classes in JS, we mimic this behavior or this ability to create objects out of templates with the use of constructor functions

function  Pokemon (name, type, lvl){
    this.name = name;
    this. type = type;
    this.lvl = lvl;
};

//ES6 Class Creation

class Car {
    constructor( brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}
let car1 = new Car ("Toyota", "Vios", "2002")
console.log(car1);

let car2 = new Car ("Cooper", "Mini", "1969")
console.log(car2);